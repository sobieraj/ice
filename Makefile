all: server client

Calc.cpp Calc.h: Calc.ice

	slice2cpp Calc.ice



server: Calc.cpp Server.cpp

	g++ -I. Calc.cpp Server.cpp -lIce -lIceUtil -o server -pthread



client: Calc.cpp Client.cpp

	g++ -I. Calc.cpp Client.cpp -lIce -lIceUtil -o client -pthread

	



clean:

	rm -rf *.o Calc.cpp Calc.h server client