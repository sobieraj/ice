#include <Ice/Ice.h>

#include <Calc.h>

#include <cmath>

using namespace std;

using namespace Demo;

 

class CalcI : public Calc {

public:

    virtual double sqrt(const double value, const Ice::Current&);

};

 

double 

CalcI::

sqrt(const double value, const Ice::Current&)

{

cout << "client : " << value << endl;

	return std::sqrt(value);

}

 

int

main(int argc, char* argv[])

{

    int status = 0;

    Ice::CommunicatorPtr ic;

    try {

        ic = Ice::initialize(argc, argv);

        Ice::ObjectAdapterPtr adapter =

            ic->createObjectAdapterWithEndpoints("SimpleCalcAdapter", "default -p 10000");

        Ice::ObjectPtr object = new CalcI;

        adapter->add(object, ic->stringToIdentity("SimpleCalc"));

        adapter->activate();

        ic->waitForShutdown();

    } catch (const Ice::Exception& e) {

        cerr << e << endl;

        status = 1;

    } catch (const char* msg) {

        cerr << msg << endl;

        status = 1;

    }

    if (ic) {

        try {

            ic->destroy();

        } catch (const Ice::Exception& e) {

            cerr << e << endl;

            status = 1;

        }

    }

    return status;

}